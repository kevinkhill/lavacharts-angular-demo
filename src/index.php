<?php
    error_reporting(E_ALL);

    require('../vendor/autoload.php');

    $lava = new \Khill\Lavacharts\Lavacharts;

    $employeeCount = rand(2,6);

    $salesTable = $lava->DataTable();
    $salesTable->addDateColumn('Date');

    for ($a=0; $a <= $employeeCount; $a++) {
        $salesTable->addNumberColumn('Employee'.$a);
    }

    for ($g=1;$g < 30; $g++) {
        $rowData = ['2016-1-'.$g];

        for ($b=0; $b <= $employeeCount; $b++) {
            $rowData[] = rand(550000,1000000);
        }

        $salesTable->addRow($rowData);
    }

    $lava->LineChart('Sales', $salesTable, [
        'elementId' => 'my-chart',
        'legend' => 'bottom'
    ]);
?>

<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Lavacharts + Angular</title>
  <base href="/">

  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" type="image/x-icon" href="favicon.ico">
</head>
<body>
    <?= $lava->renderAll([
        'auto_run' => false
    ]); ?>

    <app-root>Lavacharts + Angular is loading...</app-root>

</body>
</html>
