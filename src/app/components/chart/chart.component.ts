import { OnInit, AfterViewInit, Component} from '@angular/core';
import {LavaJsService} from '../../../../../../projects/lavacharts/lavacharts/javascript/src/angular/LavaJsService';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.css']
})
export class ChartComponent implements OnInit, AfterViewInit {
  private lava: any;

    /**
    * @param {LavaJsService} lavaJs
    */
    constructor(private lavaJs: LavaJsService) {
        this.lava = lavaJs.getInstance();
    }

    ngOnInit() {
        console.log('Chart component loaded.');
    }

    ngAfterViewInit() {
        const lava = this.lava;

        console.log(`Lava.js v${lava.VERSION}`);

        lava.run();

        lava.ready(() => {
            lava.getChart('Sales', function (chart) {
                console.log(chart);
            });
        });
    }
}
