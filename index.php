<?php
    require('vendor/autoload.php');

    $lava = new Khill\Lavacharts\Lavacharts;

    $employeeCount = rand(2,6);

    $salesTable = $lava->DataTable();
    $salesTable->addDateColumn('Date');

    for ($a=0; $a <= $employeeCount; $a++) {
        $salesTable->addNumberColumn('Employee'.$a);
    }

    for ($g=1;$g < 30; $g++) {
        $rowData = ['2016-1-'.$g];

        for ($b=0; $b <= $employeeCount; $b++) {
            $rowData[] = rand(550000,1000000);
        }

        $salesTable->addRow($rowData);
    }

    $lava->LineChart('Sales', $salesTable, [
        'elementId' => 'my-chart',
        'legend' => 'bottom'
    ]);
?>

<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Demo</title>
  <base href="/">

  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" type="image/x-icon" href="favicon.ico">
</head>
<body>
  <app-root></app-root>

  <?= $lava->lavajs(); ?>

  <script type="text/javascript" src="static/inline.bundle.js"></script>
  <script type="text/javascript" src="static/polyfills.bundle.js"></script>
  <script type="text/javascript" src="static/styles.bundle.js"></script>
  <script type="text/javascript" src="static/vendor.bundle.js"></script>
  <script type="text/javascript" src="static/main.bundle.js"></script>

  <?= $lava->renderAll(); ?>
</body>
</html>
