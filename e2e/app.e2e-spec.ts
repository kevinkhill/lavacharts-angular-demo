import { Angular.DevPage } from './app.po';

describe('angular.dev App', () => {
  let page: Angular.DevPage;

  beforeEach(() => {
    page = new Angular.DevPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
